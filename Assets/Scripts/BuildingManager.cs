﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Animator))]
public class BuildingManager : MonoBehaviour {
    public int maxDurability = 100;
    public LayerMask layerMask;
    public Slider slider;
    public int durability = 100;

    public GameObject gameObjectToDestroy;

    public List<Transform> elements;
    
    private Animator animator;

    private bool isDestroyed;
    private float timer;

    private void Start() {
        durability = maxDurability;

        animator = GetComponent<Animator>();
        slider.maxValue = maxDurability;
        slider.value = durability;
    }
    
    private void Update() {
        if (isDestroyed) {
            timer += Time.deltaTime;

            if (timer >= 3) {
                Destroy(gameObject);
            }

            return;
        }

        foreach (var element in elements) {
            element.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;
        }
    }
    
    public void TakeDamage(int damage) {
        if (isDestroyed)
            return;
        
        //int amount = durability % 5 - (durability - damage) % 5;
        var elementsNotDestroyed = elements.FindAll(element => element.GetComponent<BuildingElement>().isDestroyed == false);

        if (elements.Count != 0) {
            int damageToDestroyElement = maxDurability / elements.Count;

            if (damageToDestroyElement == 0)
                return;

            int elementBefore = durability / damageToDestroyElement;
            int elementAfter = (durability - damage) / damageToDestroyElement;

            int amount = elementBefore - elementAfter;

            for (int i = 0; i < amount; i++) {
                if (elementsNotDestroyed.Count == 0) {
                    break;
                }

                int randomIndex = Random.Range(0, elementsNotDestroyed.Count - 1);
                elementsNotDestroyed[randomIndex].GetComponent<BuildingElement>().DestroyElement();

                elementsNotDestroyed.RemoveAt(randomIndex);
            }
        }

        durability -= damage;
        animator.SetTrigger("Damage");
        
        slider.value = durability;

        if (durability <= 0)
            DestroyBuilding();
    }

    public void Repair(int power) {
        if (isDestroyed)
            return;
        
        int amount = 1;

        var elementsDestroyed = elements.FindAll(element => element.GetComponent<BuildingElement>().isDestroyed);

        for (int i = 0; i < amount; i++) {
            if (elementsDestroyed.Count == 0) {
                break;
            }

            int randomIndex = Random.Range(0, elementsDestroyed.Count - 1);
            elementsDestroyed[randomIndex].GetComponent<BuildingElement>().RepairElement();

            elementsDestroyed.RemoveAt(randomIndex);
        }
        
        durability += power;
        
        slider.value = durability;

        if (durability > maxDurability)
            durability = maxDurability;
    }

    protected virtual void DestroyBuilding() {
        var elementsNotDestroyed = elements.FindAll(element => element.GetComponent<BuildingElement>().isDestroyed == false);

        foreach (var element in elementsNotDestroyed) {
            element.GetComponent<BuildingElement>().DestroyElement();
        }

        isDestroyed = true;
        
        Destroy(gameObjectToDestroy);
    }
}