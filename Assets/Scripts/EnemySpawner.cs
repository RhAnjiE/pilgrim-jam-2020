﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour {
    public float secondsToNextSpawn = 5;
    public GameObject enemyPrefab;
    public GameObject target;

    private float timerPower = 0;
    
    private float timer = 0f;

    void Update() {
        timerPower += Time.deltaTime;
        timer += Time.deltaTime;

        if (!(timer >= secondsToNextSpawn))
            return;
        
        timer = 0;
        Spawn();
    }

    private void Spawn() {
        var enemy = Instantiate(enemyPrefab, transform).GetComponent<Enemy>();
        enemy.target = target;
        enemy.speed = Random.Range(1, 3);
        enemy.attackSpeed = Random.Range(1, 3) + (int)(timerPower / 20);
    }
}
