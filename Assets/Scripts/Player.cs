﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour {
    public float maxHealth = 100;
    public int speed = 2;
    public int power = 3;
    public LayerMask enemyLayerMask;
    public LayerMask repairableLayerMask;
    public GameObject deadPanel;
    public TextMeshProUGUI deadText;
    public TextMeshProUGUI loseText;

    public float gameTime = 0;
    
    public float deadTime = 5;
    private bool isDead = false;
    private float deadTimer = 0;
    private Vector3 spawnPosition;
    
    public bool lostGame = false;
    //Dźwięk uderzenia 
    //File name: 9876__heigh-hoo__car-door-closed.mp3
    public AudioSource buildingSound;

    //File name: 412430__1histori__small-knife-slide.mp3
    public AudioSource swordSound;

    //File name: 129346__snaginneb__male-grunt.mp3
    public AudioSource hitEnemy;

    public GameObject attackPoint;
    
    public float health = 100;
    public int killedEnemyAmount = 0;
    
    private SpriteRenderer spriteRenderer;
    private Animator animator;
    
    public float attackSpeed = 2f;
    private float timer = 2f;

    private bool isMoving = false;
    
    private void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();

        health = maxHealth;
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown("joystick button 1")) {
            SceneManager.LoadScene("Play");
        }
        
        if (lostGame)
            return;
        
        gameTime += Time.deltaTime;

        loseText.text = $"Your village is destroyed!\nYou survived {(int)gameTime} second!";
        
        if (timer <= attackSpeed)
            timer += Time.deltaTime;

        if (isDead) {
            deadTimer -= Time.deltaTime;
            deadText.text = $"You died.\nWait {(int)deadTimer + 1} to respawn.";

            if (deadTimer <= 0) {
                isDead = false;

                gameObject.GetComponent<Collider2D>().enabled = true;
                transform.position = spawnPosition;
                deadPanel.SetActive(false);
                health = maxHealth;
            }

            return;
        }
        
        var horizontalMove = Input.GetAxis("Horizontal");

        if (Mathf.Abs(horizontalMove) > 0.1f) {
            transform.position -= Vector3.left * (horizontalMove * speed * Time.deltaTime);

            //Obracanie postaci w kierunku chodzenia
            transform.rotation = Quaternion.Euler(0, horizontalMove >= 0.1 ? 1 : -1 * 180, 0);
            
            animator.SetFloat("Speed", Mathf.Abs(horizontalMove));
            isMoving = true;
        }

        else {
            animator.SetFloat("Speed", 0);
            isMoving = false;
        }

        if (Input.GetKeyDown(KeyCode.Q)||Input.GetKeyDown("joystick button 0")) {
            Attack();
        }
    }

    private void Attack() {
        if (timer < attackSpeed) {
            return;
        }

        timer = 0;
        animator.SetTrigger("Attack");

        bool isEnemyFound = false;

        Collider2D[] hitEnemies = Physics2D.OverlapCircleAll(attackPoint.transform.position, 0.8f, enemyLayerMask);

        foreach (var enemy in hitEnemies) {
            Enemy obj = enemy.GetComponent<Enemy>();
            if ( obj != null) {
                killedEnemyAmount += 1;
                
                isEnemyFound = true;
                hitEnemy.Play();
                obj.Die();
            }
        }

        if (!isMoving && !isEnemyFound) {
            Collider2D[] hitBuildings = Physics2D.OverlapCircleAll(attackPoint.transform.position, 0.8f, repairableLayerMask);
            buildingSound.Play();
            foreach (var obj in hitBuildings) {
                var building = obj.GetComponent<BuildingManager>();
                if (building != null) {
                    building.Repair(power);
                }
            }
        } else {
            swordSound.Play();
        }
    }

    public void TakeDamage(int damage) {
        health -= damage;

        if (health <= 0)
            Die();
    }

    private void Die() {
        isDead = true;
        deadTimer = deadTime;
        health = 0;

        gameObject.GetComponent<Collider2D>().enabled = false;
        deadPanel.SetActive(true);
        
        spawnPosition = transform.position;
    }

    private void OnDrawGizmosSelected() {
        Gizmos.DrawWireSphere(attackPoint.transform.position, 0.5f);
    }
}
