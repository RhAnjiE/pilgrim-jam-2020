﻿using UnityEngine;

public class MainBuildingManager: BuildingManager { 
    public GameObject losePanel;
    public Player player;
    protected override void DestroyBuilding() {
        player.lostGame = true;
        losePanel.SetActive(true);
        
        base.DestroyBuilding();
    }
}