﻿using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class BackgroundElement : MonoBehaviour{
    public float speed;
    public Camera target;
    
    private Vector3 lastCameraPosition;
    private float textureUnitWidth;
    private SpriteRenderer spriteRenderer;

    public void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        var sprite = spriteRenderer.sprite;
        
        textureUnitWidth = sprite.texture.width / sprite.pixelsPerUnit;
    }

    public void Update() {
        var cameraPosition = target.transform.position;
        
        Vector3 deltaMovement = cameraPosition - lastCameraPosition;
        Vector3 position = transform.position;
        position += new Vector3(deltaMovement.x * speed, deltaMovement.y);

        lastCameraPosition = cameraPosition;
        
        if (Mathf.Abs(cameraPosition.x - transform.position.x) >= textureUnitWidth) {
            float offset = (cameraPosition.x - position.x) % textureUnitWidth;
            
            position = new Vector3(cameraPosition.x + offset, position.y);
        }

        transform.position = position;
    }
}