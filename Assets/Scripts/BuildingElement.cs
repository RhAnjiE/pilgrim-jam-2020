﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PolygonCollider2D))]
public class BuildingElement : MonoBehaviour {
    [HideInInspector]
    public Vector3 startPosition;
    [HideInInspector]
    public Quaternion startRotation;
    public bool isDestroyed;

    private Rigidbody2D rigidbody;
    private PolygonCollider2D polygonCollider;

    private void Start() {
        startPosition = transform.position;
        startRotation = transform.rotation;

        rigidbody = GetComponent<Rigidbody2D>();
        polygonCollider = GetComponent<PolygonCollider2D>();

        rigidbody.gravityScale = 0;
        polygonCollider.enabled = false;
    }

    public void DestroyElement() {
        if (isDestroyed)
            return;
        
        isDestroyed = true;
        rigidbody.gravityScale = 1;
        polygonCollider.enabled = true;
        
        rigidbody.AddForce(new Vector2(Random.Range(-2, 2), Random.Range(2, 4)), ForceMode2D.Impulse);
        rigidbody.AddTorque(Random.Range(-2, 2) / 100f, ForceMode2D.Impulse);
 
    }

    public void RepairElement() {
        if (!isDestroyed)
            return;
        
        isDestroyed = false;

        rigidbody.velocity = Vector2.zero;
        rigidbody.angularVelocity = 0;

        transform.position = startPosition;
        transform.rotation = startRotation;

        rigidbody.gravityScale = 0;
        polygonCollider.enabled = false;
    }
}