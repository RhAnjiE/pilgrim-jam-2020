﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Castle : MonoBehaviour
{
    public double durability = 1000;
    public int max_durability = 1000;
    public double defence = 10; 
    public Slider slider;
    // Start is called before the first frame update
    void Start()
    {
        durability = max_durability;
        defence = 0.7*durability;
        slider.value = max_durability;
       // slider.max_value = max_durability;
    }

    // Update is called once per frame
    void Update()
    {
        defence = 0.7*durability;
      //  slider.value = Int32.Parse(durability);
    }
}
