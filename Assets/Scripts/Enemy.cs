﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Animator))]
public class Enemy : MonoBehaviour {
    public int maxHealth = 100;
    public float speed = 2f;
    public int power = 3;
    public GameObject target;
    
    public int health = 100;

    public float attackSpeed = 1f;
    private float timer = 2f;

    private bool isAttacking = false;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    private void Start() {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }
    
    private void Update() {
        if (timer <= attackSpeed)
            timer += Time.deltaTime;

        if (!isAttacking) {
            if ((int) transform.position.x != (int) target.transform.position.x) {
                float direction = (transform.position.x - target.transform.position.x) > 0f ? 1 : -1;

                transform.rotation = Quaternion.Euler(0, (direction >= 1 ? 1 : 0) * 180, 0);

                transform.position += Vector3.left * (direction * speed * Time.deltaTime);
                animator.SetFloat("Speed", speed);

                return;
            }
        }

        animator.SetFloat("Speed", 0);
    }

    private bool Attack() {
        if (timer >= attackSpeed) {
            animator.SetTrigger("Attack");
            isAttacking = true;

            timer = 0;
            return true;
        }

        return false;
    }

    private void OnCollisionStay2D(Collision2D other) {
        var building = other.gameObject.GetComponent<BuildingManager>();
        if (building != null) {
            if (Attack())
                building.TakeDamage(power);
        }
        
        var player = other.gameObject.GetComponent<Player>();
        if (player != null) {
            if (Attack())
                player.TakeDamage(power);
        }
    }

    private void OnCollisionExit2D(Collision2D other) {
        isAttacking = false;
    }

    public void Die() {
        Destroy(gameObject);
    }
}
