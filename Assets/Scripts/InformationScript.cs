﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class InformationScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown || Input.GetKeyDown("joystick button 0") || Input.GetKeyDown("joystick button 1")) {
            SceneManager.LoadScene("Game");
        }
    }
}
