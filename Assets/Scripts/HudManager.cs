﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HudManager : MonoBehaviour {
    public TextMeshProUGUI healthText;
    public TextMeshProUGUI killedEnemyText;
    public TextMeshProUGUI timeText;

    public Player player;
    //public GameManager gameManager;
    
    void Start()
    {
        
    }

    void Update() {
        healthText.text = $"Health: {player.health}/{player.maxHealth}";
        killedEnemyText.text = $"Killed enemy: {player.killedEnemyAmount}";
        timeText.text = $"Time: {(int)player.gameTime}";
    }
}
