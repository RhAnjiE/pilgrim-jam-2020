﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LosePanelManager : MonoBehaviour{
    public void RepeatGame() { 
        SceneManager.LoadScene("Game");
    }
        
    public void BackToMainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}